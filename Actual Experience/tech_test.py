import pycurl
import base64
import time

url_string = 'http://authenticationtest.herokuapp.com/login/'
login_ulr = url_string + 'ajax/'
logged_in = 'http://authenticationtest.herokuapp.com'
username = 'testuser'
password = '54321password12345'
user_password = base64.b64encode('{}:{}'.format(username, password).encode())
headers = {'Authorization': 'Basic {}'.format(user_password)}

''' Deal with python 2/3 compatibity'''
try:
    from StringIO import StringIO as CurlBuffer
except ImportError:
    from io import BytesIO as CurlBuffer

try:
    from urllib.parse import urlencode
except ImportError:
    # python 2
    from urllib import urlencode


def get_token():
    with open('cookie.txt') as f:
        a = f.readlines()

    for line in a:
        if 'authenticationtest.herokuapp.com' in line:
            return(line.split('\t')[-1][:-1])

    raise Exception('FAILED to get token')


def t():
    curl_buffer = CurlBuffer()
    curl_buffer_2 = CurlBuffer()
    curl_buffer_3 = CurlBuffer()

    c = pycurl.Curl()

    c.setopt(c.URL, url_string)
    c.setopt(pycurl.VERBOSE, 1)
    c.setopt(pycurl.COOKIEJAR, 'cookie.txt')
    c.setopt(pycurl.COOKIEFILE, 'cookie.txt')
    token = get_token()

    c.setopt(c.WRITEDATA, curl_buffer)
    c.setopt(c.HEADERFUNCTION, curl_buffer.write)
    c.perform()

    c.setopt(c.URL, login_ulr)
    c.setopt(pycurl.COOKIEJAR, 'cookie.txt')
    c.setopt(pycurl.COOKIEFILE, 'cookie.txt')
    t = int(time.time())
    c.setopt(pycurl.COOKIE, '_ga=GA1.3.1673135797.{}; _next_=root;'.format(t))

    post_data = {'csrfmiddlewaretoken': token,
                 'username': username,
                 'password': password}

    postfields = urlencode(post_data)
    c.setopt(c.POSTFIELDS, postfields)

    c.setopt(c.WRITEDATA, curl_buffer_2)
    c.setopt(c.HEADERFUNCTION, curl_buffer_2.write)
    c.setopt(c.FOLLOWLOCATION, True)
    c.perform()

    c.setopt(pycurl.COOKIEJAR, 'cookie.txt')
    c.setopt(pycurl.COOKIEFILE, 'cookie.txt')

    # c.setopt(pycurl.COOKIE, '_ga=GA1.3.1673135797.{}; _next_=root;'.format(t))

    c.setopt(c.URL, logged_in)
    c.setopt(c.WRITEDATA, curl_buffer_3)
    c.perform()
    # print(c.getinfo(pycurl.HTTP_CODE), c.getinfo(pycurl.EFFECTIVE_URL))
    c.close()

    print('----------------------------------------------------------------')
    print(curl_buffer.getvalue())
    print('----------------------------------------------------------------')
    print(curl_buffer_2.getvalue())
    print('----------------------------------------------------------------')
    print(curl_buffer_3.getvalue())
    # odZiCg6eZYqpfvCvszl4SYoBvGjah9f7


if __name__ == '__main__':
    t()
